# A record in the log file always begins with a line starting with the
# string "Started" and ends with a line starting with the string "Completed"


class PageHit

  attr_accessor :verb, :uri, :ip, :datetime, :controller, :action, :params, :status

  def initialize()
  end

  def within_date_range(start_date, end_date)
    return (@datetime >= start_date) && (@datetime <= end_date)
  end
end
