# real    56m49.516s
# user    56m39.203s
# sys     0m7.998s

require './page_hit'
require 'set'
require 'csv'

COMMON_YEAR_DAYS_IN_MONTH = [nil, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def days_in_month(month, year = Time.now.year)
     return 29 if month == 2 && Date.gregorian_leap?(year)
        COMMON_YEAR_DAYS_IN_MONTH[month]
end

class Analyzer

  attr_reader :unique_ips

  STARTED_REGEX    = /^Started (?<verb>GET|POST|PATCH|PUT|DELETE|OPTIONS|PROPFIND|SEARCH|HEAD) \"(?<uri>.*)\" for (?<ip>[0-9\.]*) at (?<datetime>.*)$/
  PROCESSING_REGEX = /^  Processing by (?<controller>.*)\#(?<action>.*) as/
  PARAM_REGEX      = /^  Parameters: {(?<params>.*)}$/
  COMPLETED_REGEX  = /^Completed (?<status>[0-9]*) .* in [0-9]*ms/

  def initialize(options)
    @start_date        = options[:start_date]
    @end_date          = options[:end_date]
    @paths             = options[:logfiles]
    @temp_ph           = nil
    @line_number       = 0
    @sorted_pagehits     = autovivifying_hash

    @paths.each do |path|
      raise ArgumentError unless File.exists?(path)
    end

    @page_hits  = []
    @unique_ips = [].to_set

    process_logs()
  end

  def sort_page_hit_into_buckets(ph)
    if ph.within_date_range(@start_date, @end_date)
      # All-time.
      @page_hits << ph

      # Hourly
      sort_into_hourly_bucket(ph)
    else
      STDERR.puts "Page hit is out of date range."
    end
  end

  def autovivifying_hash
    Hash.new {|ht,k| ht[k] = autovivifying_hash}
  end

  def sort_into_hourly_bucket(ph)
    # Check if the minute bucket exists yet.
    if @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day].has_key?(ph.datetime.hour)
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:ip] << ph.ip
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:count] += 1
      # Create it if it doesn't.
    else
      # We keep duplicate IPs for now...we will cast to set later to take out dupe's.
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:ip]             = [ph.ip]
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:count]          = 1
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:status_success] = 0
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:status_error]   = 0
    end

    # Record whether success or failure.
    # 2xx or 3xx are success.
    # 4xx or 5xx are errors.
    if ph.status.start_with?("2") || ph.status.start_with?("3")
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:status_success] += 1
    elsif ph.status.start_with?("4") || ph.status.start_with?("5")
      @sorted_pagehits[ph.datetime.year][ph.datetime.month][ph.datetime.day][ph.datetime.hour][:status_error] += 1
    end

    # Capture IP.
    @unique_ips << ph.ip
  end

  def process_logs
    @paths.each do |file|
      File.open(file, 'r') do |f|
        process_file(f)
      end
    end
  end

  def process_file(file)
    file.each_line do |line|
      @line_number += 1
      STDERR.puts @line_number if @line_number % 100000 == 0

      next unless re_match = line.match(Regexp.union([STARTED_REGEX, PROCESSING_REGEX,
                                                      PARAM_REGEX, COMPLETED_REGEX]))

      if line.match(STARTED_REGEX)
        @temp_ph = PageHit.new
        @temp_ph.verb     = re_match[:verb]
        @temp_ph.uri      = re_match[:uri]
        @temp_ph.ip       = re_match[:ip]
        @temp_ph.datetime = DateTime.strptime(re_match[:datetime], '%Y-%m-%d %H:%M:%S %z')
      end

      if line.match(PROCESSING_REGEX)
        begin
          @temp_ph.controller = re_match[:controller]
          @temp_ph.action     = re_match[:action]
        rescue Exception => e
          STDERR.puts "Problem on line #{@line_number}: #{e}"
        end
      end

      if line.match(PARAM_REGEX)
        begin
          @temp_ph.params = re_match[:params]
        rescue Exception => e
          STDERR.puts "Problem on line #{@line_number}: #{e}"
        end
      end

      if line.match(COMPLETED_REGEX)
        begin
          @temp_ph.status = re_match[:status]
          sort_page_hit_into_buckets(@temp_ph) unless @temp_ph.nil?
          @temp_ph = nil
        rescue Exception => e
          STDERR.puts "Problem on line #{@line_number}: #{e}"
        end
      end
    end
  end

  def hourly_statistics
    hourly_stats = []
    @sorted_pagehits.each do |year, months|
      months.each do |month, days|
        days.each do |day, hours|
          hours.each do |hour, v|
            hourly_visits = OpenStruct.new
            hourly_visits.year            = year
            hourly_visits.month           = month
            hourly_visits.day             = day
            hourly_visits.hour            = hour
            hourly_visits.page_hit_count  = 0
            hourly_visits.days_count      = 0
            hourly_visits.status_success  = 0
            hourly_visits.status_error    = 0
            hourly_visits_visitor_ips     = []

            hourly_visits.page_hit_count += v[:count]
            hourly_visits.status_success += v[:status_success]
            hourly_visits.status_error   += v[:status_error]

            hourly_stats << hourly_visits
          end
        end
      end
    end

    return hourly_stats
  end

  def monthly_statistics
    monthly_stats = []
    @sorted_pagehits.each do |year, months|
      months.each do |month, days|
        monthly_visits = OpenStruct.new
        monthly_visits.year            = year
        monthly_visits.month           = month
        monthly_visits.page_hit_count  = 0
        monthly_visits.days_count      = 0
        monthly_visits.status_success  = 0
        monthly_visits.status_error    = 0
        monthly_visits_visitor_ips     = []

        days.each do |day, hours|
          monthly_visits.days_count += 1
          hours.each do |hour, v|
            monthly_visits_visitor_ips     = monthly_visits_visitor_ips.concat(v[:ip])
            monthly_visits.page_hit_count += v[:count]
            monthly_visits.status_success += v[:status_success]
            monthly_visits.status_error   += v[:status_error]
          end
        end

        # Divide page hit count by number of days.
        monthly_visits.average_per_day         = monthly_visits.page_hit_count/monthly_visits.days_count

        # Take the length of the unique visitor IPs.
        monthly_visits.unique_visitors         = monthly_visits_visitor_ips.to_set.length

        # Divide the number of unique visitors by number of days.
        monthly_visits.average_visitor_per_day = monthly_visits.unique_visitors/monthly_visits.days_count

        # Come up with list of IPs that visited only once.
        single_visit_list = []
        seen_visit_list   = []
        monthly_visits_visitor_ips.each do |ip|
          if seen_visit_list.include?(ip)
            single_visit_list.delete(ip)
          else
            single_visit_list << ip
            seen_visit_list << ip
          end
        end
        monthly_visits.single_visit_count = single_visit_list.length
        monthly_visits.multi_visit_count  = (seen_visit_list - single_visit_list).to_set.length

        # Divide status success count by page hit count times 100.
        monthly_visits.success_as_pct = monthly_visits.status_success.to_f/monthly_visits.page_hit_count.to_f*100

        monthly_stats << monthly_visits
      end
    end

    return monthly_stats
  end

  def pretty_print_monthly(monthly_stats)
    header = [
      'Year',
      'Month',
      'Visits',
      'Average # of visitors per day',
      '# of unique visitors',
      '# of visitors who visited once',
      '# of visitors who visited more than once',
      'Total hits',
      'Successful hits (%)'
    ]
    CSV.open(File.join("output", "monthly_stats.csv"), "wb") do |csv|
      csv << header
      monthly_stats.each do |ms|
        row = [
          ms.year,
          ms.month,
          ms.page_hit_count/8,
          ms.average_visitor_per_day,
          ms.unique_visitors,
          ms.single_visit_count,
          ms.multi_visit_count,
          ms.page_hit_count,
          ms.success_as_pct
        ]
        csv << row
      end
    end
  end

  def pretty_print_hourly(hourly_stats)
    header = [
      'Year',
      'Month',
      'Day',
      'Hour',
      'Page hits'
    ]
    CSV.open(File.join("output", "hourly_stats.csv"), "wb") do |csv|
      csv << header
      hourly_stats.each do |hs|
        row = [
          hs.year,
          hs.month,
          hs.day,
          hs.hour,
          hs.page_hit_count
        ]
        csv << row
      end
    end
  end

  def pretty_print_unique_ips
    header = [
      'IP Address'
    ]
    CSV.open(File.join("output", "ip_list.csv"), "wb") do |csv|
      csv << header
      @unique_ips.each do |ip|
        row = [
          ip
        ]
        csv << row
      end
    end
  end
end
