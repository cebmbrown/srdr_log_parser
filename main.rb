require './analyzer'
require 'optparse'
require 'optparse/date'
require 'ostruct'
require 'date'
require 'pp'

def main(options)
  anal = Analyzer.new(options)

  monthly_stats = anal.monthly_statistics
  anal.pretty_print_monthly(monthly_stats)

  hourly_stats  = anal.hourly_statistics
  anal.pretty_print_hourly(hourly_stats)

  anal.pretty_print_unique_ips
end

if __FILE__ == $0

  class Options

    def self.parse(args)
      # The options specified on the command line will be collected in *options*.
      # We set default values here.
      options = OpenStruct.new
      options.start_date = DateTime.strptime('2016-01-01 00:00:00 +0000', '%Y-%m-%d %H:%M:%S %z')
      options.end_date   = DateTime.now
      options.encoding   = 'utf8'
      options.verbose    = false

      opts = OptionParser.new do |opts|
        opts.banner = "Usage: main.rb [options]"

        opts.separator ""
        opts.separator "Specific options:"

        # Begin collecting page hits from this date.
        opts.on("-s", "--startdate [DATETIME]", DateTime, "Start date") do |time|
          options.start_date = time
        end

        # Stop collecting page hit on this date.
        opts.on("-e", "--enddate [DATETIME]", DateTime, "End date") do |time|
          options.start_date = time
        end

        # List of arguments.
        opts.on("-l", "--logfiles x,y,z", Array, "Paths to log files separated by ','") do |list|
            options.logfiles = list
        end
      end # opts = OptionParse.new do |opts|

      opts.parse!(args)
      options
    end # def self.parse(args)
  end # class Options

  options = Options.parse(ARGV)
  if options[:logfiles].nil?
    puts "You have to supply a list of logfiles."
    puts "For more help Use: ruby main.rb --help"
  else
    main(options)
  end
end
